module.exports = {
  baseUrl         : '/',
  devServer       : {
    disableHostCheck: true
  },
  publicPath      : '/'
};