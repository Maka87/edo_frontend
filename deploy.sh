echo ${CI_REGISTRY_PASSWORD} | sudo docker login -u ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}
sudo docker-compose -f docker-compose_edo_frontend.yml pull edo_frontend
sudo docker-compose -f docker-compose_edo_frontend.yml stop edo_frontend
sudo docker-compose -f docker-compose_edo_frontend.yml up -d
