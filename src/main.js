import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import VueCookies from 'vue-cookies';
import Vuetify from 'vuetify';
import { VueMaskDirective } from 'v-mask'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import 'leaflet/dist/leaflet.css';
import { Icon } from 'leaflet';
delete Icon.Default.prototype._getIconUrl;

Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

Vue.use(Vuetify, {
  primary  : '#1976D2',
  secondary: '#424242',
  accent   : '#82B1FF',
  error    : '#FF5252',
  info     : '#2196F3',
  success  : '#4CAF50',
  warning  : '#FFC107'
});

Vue.use(VueCookies);
Vue.config.productionTip = true;
Vue.directive('mask', VueMaskDirective);

new Vue({
  VueCookies,
  vuetify : new Vuetify(),
  store,
  router,
  render : h => h(App),
}).$mount('#app');
