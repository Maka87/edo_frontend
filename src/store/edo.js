import axios from 'axios'

export default {
  state: {
    client: {},
    services: [],
    devices: [],
    processing: false,
    error: null,
    page: 1,
    pdf: '',
    data: []
  },
  mutations: {
    SET_PROCESSING(state, payload) {
      state.processing = payload;
    },
    SET_ERROR(state, payload) {
      state.error = payload;
    },
    CLEAN_ERROR(state) {
      state.error = null;
    },
    SET_CLIENT(state, payload) {
      state.client = payload;
    },
    SET_SERVICES(state, payload) {
      state.services = payload;
    },
    SET_DEVICES(state, payload) {
      state.devices = payload;
    },
    SET_PAGE(state, payload) {
      state.page = payload;
    },
    SET_PDF(state, payload) {
      state.pdf = payload;
    },
    SET_DATA(state, payload) {
      state.data = payload;
    }
  },
  getters: {
    getError: (state) => state.error,
    getClient: (state) => state.client,
    getService: (state) => state.services,
    getDevices: (state) => state.devices,
    getProcessing: (state) => state.processing,
    getPage: (state) => state.page,
    getPdf: (state) => state.pdf,
    getData: (state) => state.data,
  },
  actions: {
    getInfo: async ({commit}, payload) => {
      commit('SET_PROCESSING', true);
      let responce = await axios.get(process.env.VUE_APP_SRVBACKEND + 'form/?hash='+payload.hash);
      if(responce.status == '200') {
        if(!responce.data.error) {
          commit("SET_CLIENT", responce.data.clientInfo)
          if (responce.data.sms_send && responce.data.sms_send == true && !responce.data.registartion) {
            commit("SET_DATA", responce.data.data);
            commit("SET_PAGE", 2);
          } else if (responce.data.registartion) {
            commit("SET_DATA", responce.data.data);
            commit("SET_PAGE", 3);
          } else {
            commit("SET_SERVICES", responce.data.services.services)
            commit("SET_DEVICES", responce.data.services.devices)
          }
        } else {
          commit('SET_ERROR', responce.data.error);
        }
      }
      commit('SET_PROCESSING', false);
    },
    setPage: async ({commit}, payload) => {
      commit("SET_PAGE", payload.page);
    },
    setError: async ({commit}, payload) => {
      commit("SET_ERROR", payload.error);
    },
    setPDF: async ({commit}, payload) => {
      commit("SET_PDF", `${process.env.VUE_APP_SRVBACKEND}offer_pdf?hash=${payload.hash}`)
    },
    setDATA: async ({commit}, payload) => {
      commit("SET_DATA", payload)
    }
  }
}