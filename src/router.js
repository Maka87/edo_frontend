import Vue from 'vue';
import Router from 'vue-router';
import axios from 'axios';
import VueCookies from 'vue-cookies';
import Edo from './view/Edo.vue';
Vue.use(Router);

export default new Router({
  hashbang        : false,
  history         : true,
  mode            : 'history',
  linkActiveClass : 'active',
  transitionOnLoad: true,
  base            : '/',
  routes          : [
    {
      path       : '/',
      name       : 'Edo',
      component  : Edo,
      meta: {title: 'Электронный документооборот'},
      beforeEnter: async (to, from, next) => {
        document.title = to.meta.title;
        return next();
      }
    }
  ]
});
